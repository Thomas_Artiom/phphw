<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <header>
        <img  width="200px" src="logo.png" alt="logo mospolytech">
        <h1>Feedback Form</h1>
    </header>

    <main>
        <?php
            $url = "https://httpbin.org";
            $answer = get_headers($url);
        ?>

        <form class="row justify-content-center" method="" action="">
            <div class="mb-3 col-12 col-md-4">
                <label for="message-id" class="form-label">Вывод</label>
                <textarea name="message" class="form-control" id="message-id" rows="20" cols="80"><?= print_r($answer);?></textarea>
            </div>
        </form>
    </main>

    <footer>
        <p>Задание для самостоятельно работы (Собрать сайт из двух страниц.)</p>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>