<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <header>
        <img  width="200px" src="logo.png" alt="logo mospolytech">
        <h1>Feedback Form</h1>
    </header>

    <main>
        <form class="row justify-content-center" method="post" action="https://httpbin.org/post">
            <div class="mb-3 col-12 col-md-4">
                <label for="name-id" class="form-label">Имя</label>
                <input name="name" type="text" value="" class="form-control" id="name-id" placeholder="Введите ваше имя...">
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="email-id" class="form-label">Ваша почта</label>
                <input name="email" type="email" value="" class="form-control" id="email-id" placeholder="Введите вашу почту...">
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="select-id" class="form-label">Тип обращения</label>
                <div class="w-100"></div>
                <select name="select" id="select-id">
                    <option value="complaint">Жалоба</option>
                    <option value="offer">Предложение</option>
                    <option value="gratitude">Благодарность</option>
                </select>
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="message-id" class="form-label">Описание категории</label>
                <textarea name="message" class="form-control" id="message-id" rows="6"></textarea>
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="select-id" class="form-label">Тип обращения</label>
                <div class="w-100"></div>
                    <input class="form-check-input" type="checkbox" name="choice" value="SMD" id="sms-id">
                    <label class="form-check-label" for="sms-id">
                        SMS
                    </label>
                    <input class="form-check-input" type="checkbox" name="choice" value="E-mail" id="email-id">
                    <label class="form-check-label" for="email-id">
                        E-Mail
                    </label>
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <button type="submit" class="btn btn-primary">Отравить</button>
                <div class="w-100"></div>       
                <a href="header.php">2 страница</a>
            </div>
        </form>
    </main>

    <footer>
        <p>Задание для самостоятельно работы (Собрать сайт из двух страниц.)</p>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>