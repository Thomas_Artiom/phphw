<!DOCTYPE HTML>
<html lang="ru">
<head>
	<meta charset = "UTF-8">
	<link rel="stylesheet" href="styles.css" type="text/css" />
</head>
<body>
<h1>Решение уравнения (Вариант 0)</h1>
	<form action='' method='post' class="calculate-form">
		<input type="text" name="numbers1" class="numbers" placeholder="Первое число или Х">
		<span>*</span>
		<input type="text" name="numbers2" class="numbers" placeholder="Второе число или Х">
		<span>=</span>
		<input type="text" name="numbers3" class="numbers" placeholder="Чему равно">
		<input class="submit_form" type="submit" name="submit" value="Получить ответ"> 
	</form>
</body>
</html>

<?php
if(isset($_POST['submit'])){
	$number1 = $_POST['numbers1'];
	$number2 = $_POST['numbers2'];
	$number3 = $_POST['numbers3'];
	
	// Валидация
	if((!$number1 && $number1 != '0') || (!$number2 && $number2 != '0') || (!$number3 && $number3 != '0')) {
		$error_result = 'Не все поля заполнены';

	}
    else {
	    
		if($number1 == 'X' || $number1 == 'x')
		{
			$result = $number3 / $number2;
		}
		if($number2 == 'X' || $number2 == 'x')
		{ 
			$result = $number3 / $number1;
		}
		}
    if(isset($error_result)){
    	echo "<div class='error-text'>Ошибка: $error_result</div>";
    }	
    else {
	    echo "<div class='answer-text'>Ответ: $result</div>";
    }
}
?>
