<?php
    namespace MyProject\Controllers;
    use MyProject\View\View;
    use MyProject\Models\Articles\Article;
    use MyProject\Models\Users\User;
    use MyProject\Models\Comments\Comment;


    class ArticleController {
        private $view;
        private $db;
        public function __construct(){
            $this->view = new View(__DIR__.'/../../../templates');
        }

        public function edit(int $articleId): void{
            $result = Article::getById($articleId);
            if ($result === []){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
                $result->setName('Статья летняя');
                $result->setText('Жара');
            $result->save();
        }
        public function add(){
            $author = User::getById(1);
            $article = new Article();
            $article->setAuthor($author);
            $article->setName('new author');
            $article->setText('new text');
            $article->save();
            // var_dump($article);
        }
        public function delete(int $articleId): void{
            $result = Article::getById($articleId);
            if ($result === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $result->delete();
        }
    
        public function viewAll(): void {
            $articles = Article::findAll();
            $this->view->renderHTML('articles/list.php', ['articles' => $articles]);
        }
    
        public function view(int $articleID): void {
            $article = Article::getByID($articleID);
    
            if ($article === null) {
                $this->view->renderHTML('errors/404.php', [], 404);
                return;
            }
    
            $comments = Comment::findAllByOneParam('article_id', (string)$articleID);
    
            $this->view->renderHTML('articles/view.php', ['article' => $article, 'comments' => $comments]);
        }
    }
?>