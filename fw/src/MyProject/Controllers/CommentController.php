<?php

namespace MyProject\Controllers;

use MyProject\View\View;
use MyProject\Models\Articles\Article;
use MyProject\Models\Users\User;
use MyProject\Models\Comments\Comment;

class CommentController {
    private $view;

    public function __construct() {
        $this->view = new View(__DIR__ . '/../../../templates');
    }

    public function add(int $articleID): void {
        $article = Article::getByID($articleID);

        if ($article === null) {
            $this->view->renderHTML('errors/404.php', [], 404);
            return;
        }

        if (!empty($_POST)) {
            $_POST['article_id'] = $articleID;
            $comment = new Comment();
            $comment->setContent($_POST['content']);
            $comment->setAuthor(User::getById(1));
            $comment->setArticle($_POST['article_id']);
            $comment->save();
        }


        header('Location: /www/article/' . $article->getId(), true, 302);
    }

    public function edit(int $commentID): void {
        $comment = Comment::getByID($commentID);
    
        if ($comment === null) {
            $this->view->renderHTML('errors/404.php', [], 404);
            return;
        }

        if (!empty($_POST)) {
            $comment->setContent($_POST['content']);
            $article = Article::getByID($comment->getArticleId());
            $comment->save();

            header('Location: /www/article/' . $article->getID(), true, 302);
            exit();
        }

        $this->view->renderHTML('comments/edit.php', ['comment' => $comment]);
    }
}

?>