<?php
    namespace MyProject\Models\Comments;
    use MyProject\Models\Users\User;
    use MyProject\Services\Db;
    use MyProject\Models\ActiveRecordEntity;


    class Comment extends ActiveRecordEntity{
        protected $text;
        protected $authorId;
        protected $articleId;
        protected $createdAt;

        public function getName(){
            return $this->name;
        }
        public function getContent(){
            return $this->text;
        }
        public function getAuthor() {
            return User::getById($this->authorId);
        }
        public function getArticleId() {
            return $this->articleId;
        }

        public function setName(string $name){
            $this->name = $name;
        }
        public function setContent(string $text){
            $this->text = $text;
        }
        public function setAuthor(User $author){
            $this->authorId = $author->id;
        }
        public function setArticle(int $id) {
            $this->articleId = $id;
        }

        public static function getTableName():string 
        {
            return 'comments';
        }
    }
?>
