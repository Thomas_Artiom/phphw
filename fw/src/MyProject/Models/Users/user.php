<?php
    namespace MyProject\Models\Users;
    use MyProject\Models\ActiveRecordEntity;

    
    class User extends ActiveRecordEntity{
        private $nickname;

        public function getName(){
            return $this->nickname;
        }

        public static function getTableName():string 
        {
            return 'users';
        }
    }
?>