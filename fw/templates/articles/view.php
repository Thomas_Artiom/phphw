<?php include __DIR__.'/../header-article.html';?> 
    <h2><?= $article->getName()?></h2>
    <p><?= $article->getText()?></p>
    <hr>
    <div class="сomments">
            <h3 class="header">Комментарии</h3>
            <?php if (count($comments)) {
                foreach($comments as $comment) { ?>
                    <div class="comment">
                        <div class="content">
                            <a class="author">Автор: <?=$comment->getAuthor()->getName()?></a>
                            <div class="text">
                                <span class="text-comment">Комментарий: <?=$comment->getContent()?></span>
                            </div>
                            <div class="actions">
                                <a href="/www/comments/<?=$comment->getID()?>/edit" class="reply">Редактировать</a>
                            </div>
                        </div>
                    </div>
                <?php } } else { ?>
                Оставьте первый комментарий..
            <?php } ?>
            <form action="/www/article/<?=$article->getId()?>/comment" method="post" class="form">
                <div class="field">
                    <input type="text" name="article_id" value="<?=$article->getId()?>" hidden>
                    <textarea class="textarea" minlength="3" maxlength="256" name="content" cols="50"></textarea>
                </div>
                <button class="sumbit-button">
                    Добавить комментарий
                </button>
            </form>
        </div>
<?php include __DIR__.'/../footer.html';?> 