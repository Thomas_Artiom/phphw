<?php include __DIR__.'/../header-article.html';?> 
    <section class="container">
        <form action="/www/comments/<?=$comment->getID()?>/edit" class="edit-comments" method="POST">
            <div class="comments">
                <div class="comment">
                    <h2>Редактирование комментария</h2>
                    <div class="content">
                        <div class="text">
                            <p>Содержание:</p>
                        </div>
                        <div class="text">
                            <input type="text" class="textarea" required name="content" value="<?=$comment->getContent()?>">
                        </div>
                        <div class="actions">
                            <button type="submit" class="sumbit-button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <?php include __DIR__.'/../footer.html';?> 